# Python Cookbook

## Virtualenv en Virtualenvwrapper

De volgende filmjes zijn zeer handig om e.e.a. met python op een mac goed te begrijpen:

https://www.codingforentrepreneurs.com/projects/setup-python-and-django-mac/install-python-38-macos-python-installer

https://www.codingforentrepreneurs.com/projects/setup-python-and-django-mac/install-create-and-activate-virtual-environment-pi

https://www.codingforentrepreneurs.com/projects/setup-python-and-django-mac/deep-dive-virtual-environments

Om virtualenv aan de praat te krijgen heb ik het volgende gedaan:

```
brew install python
```

of

```
brew upgrade python
```

als hij al geinstalleerd is.
daarna

```
python3 -m pip3 instaal -U pip
pip install virtualenv
pip install virtualenvwrapper
```

Door die eerste regel kreeg ik pip, pip3 en pip3.7 in /usr/local/bin (die in mijn \$PATH staat)
door die tweede regel kreeg ik virtualenv in /usr/local/bin

zet het volgende in je .bashrc of .zshrc

```sh
export WORKON_HOME=~/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/local/opt/python/libexec/bin/python
#export VIRTUALENVWRAPPER_VIRTUALENV_ARGS='--no-site-packages'
## RV: het --no-site-pacakges argument was bij de laatste versie van virtualenv niet meer nodig.
export PIP_VIRTUALENV_BASE=$WORKON_HOME
export PIP_RESPECT_VIRTUALENV=true
source /usr/local/bin/virtualenvwrapper.sh
```

na de installatie van virtualenvwrapper werkte alles.
pipenv heb je dus niet nodig, dat is een alternatief voor virtualenv

Als dit nog steeds niet lukt kun je proberen je python packages op te schonen als volgt:

```
pip3 freeze | xargs pipp3 uninstall -y
pip3 freeze --user | xargs pipp3 uninstall -y --user
sudo su
pip3 freeze | xargs pipp3 uninstall -y
exit
```

Hiermee worden alle python packages die onder python3 zijn geinstalleerd verwijderd.
dan heb je een schone lei.

mocht desondanks de virtualenv binary niet in de /usr/local/bin dir terecht komen maak deze dan zelf aan met de volgende inhoud.

`/usr/local/bin/virtualenv`

```
#!/usr/local/opt/python/bin/python3.7
# -*- coding: utf-8 -*-
import re
import sys
from virtualenv.__main__ import run_with_catch
if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(run_with_catch())
```

Maak deze daarna executable:

```
chmod +x /usr/local/bin/virtualenv
```

## VSCode project setup

### Setup python sourcecode dir in een git project

Eerst maken we een nieuwe repository

```
git init projectnaam
cd projectnaam
touch README.md
git add README.md
git commit -m "add README"
```

Maak een python dir aan in de repository. De naam van de dir is arbitrair. Je zou hem bijvoorbeeld python kunnen noemen. In die python dir maak je een dir aan voor de source code van je python project, die dir krijgt dezelfde naam als je python package dat je maakt. Bijvoorbeeld

```
mkdir python
cd python
mkdir helloworld
```

Je kunt ervoor kiezen je code in de root dir van je git repository te zetten, maar als je later je project bijvoorbeeld in een docker container wilt verpakken dan wil je daar meestal een docker dir voor hebben. Door de python code in een sub dir te plaatsen wordt de code niet vermengt met andere project bestanden.

### Installeer de python plugin voor VSCode

Python plugin van Microsoft is prima

![Python plugin](img/python_plugin.png)

### Setup virtualenv in vscode

set VenV Path op
~/.virtualenvs

Deze setting kan op **user** niveau worden gezet.

![Python VenV](img/venv_path.png)

### Maak een python virtualenv

```
mkvirtualenv projectnaam
```

Selecteer de virtualenv in VSCode door een python file te openen en linksonderin op de python virtualenv te klikken, of CMD-SHIFT-P :> Python Select Interpreter.

![Select Interpreter](img/select_interpreter.png)

Als de virtual env er (nog) niet bij staat kan het helpen om VSCode opnieuw op te starten of beter nog een reload te doen CMD-SHIFT-P :> Reload Window.

Deze selectie wordt op workspace niveau opgeslagen. Dat doet VSCode door een settings.json file aan te maken in de .vscode dir in je root project dir.

![Settings.json](img/settings_json.png)

### Installeer Python development packages

Installeer deze packages in je aangemaakte virtualenv.

```
pip install autopep8 pytest setuptools
```

### Intellisense in VSCode

Doordat je de Python plugin hebt geinstalleerd en autopep8 is vscode in staat je code automatisch aan te vullen, te controleren (lint) en te formatteren (code opmaak).

Om je eigen gemaakte code ook door VSCode te laten herkennen moeten er een aantal zaken geregeld zijn.

1. Een python module wordt herkend aan een `__init__.py` bestand. Dit bestand mag leeg zijn.
   Elke dir in je python dir waar een Python module in staat moet dus een `__init__.py` bestand hebben.

2. Python zoekt modules in zijn PYTHONPATH dat is op je werk dir en tussen al je geïnstalleerde python packages. Omdat we de source code van ons project in een subdir hebben gezet zal VSCode de source code niet automatisch herkennen, omdat de werk dir van VSCode de root dir van je project is. Om er voor te zorgen dat VSCode toch je code zal herkennen gaan we de module installeren in editable mode.

### Installeren van je project als lokale python package

Om je python project te installeren maken we er een python package van. Die package kun je vervolgens installeren in je virtualenv. Hierdoor zal de code ook worden herkent in VSCode.

Om een Python Package te bouwen moeten we een "setup.py" bestand maken welke we met de python setup package kunnen installeren.

#### Python setuptools

Installeer setuptools in je virtualenv als je dat nog niet hebt gedaan.

```
pip install setuptools
```

#### setup.py

Maak in je python dir (naast de source code dir van package) een setup.py bestand aan.

Voorbeeld een project dat helloworld heet:

```python
from setuptools import setup
from subprocess import check_output
import helloworld


setup(name='hello-world',
      description='Python package with utility functions to great earth',
      version=helloworld.version,
      url='git@gitlab.com:koweblomke/python-cookbook.git',
      author='Rene Visser',
      author_email='koweblomke@gmail.com',
      license='MIT',
      packages=['helloworld'],
      entry_points={'console_scripts': ['helloworld=helloworld.__main__:main']},
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'boto3'
      ])
```

Je ziet hierboven een setup.py van een package met daar in een een aantal aandachtspunten:

- version=helloworld.version<br/>
  hiervoor moet in je `__init__.py` het volgende staan

```python
version=x.x.x
```

- packages=['helloworld']<br/>
  Dit is een lijst van al je packages. Hierin neem je al je modules op. Dat is elke subdir met een `__init__.py` bestand. Stel je hebt in de helloworld dir ook een module 'helloworld.module1' Dan is dat een subdir module1 in je source folder. met een `__init__.py`. Die neem je dan als volgt op.

```python
packages=['helloworld', 'helloworld.module1']
```

- entry_points={'console_scripts': ...<br/>
  Hiermee kun je een executable maken van je Python package. De installatie van je package zorgt ervoor dat de binary in de bin dir van je virtualenv komt te staan. Virtualenv zorgt ervoor dat de bin dir in je virtualenv in je PATH komt te staan. Bovenstaand voorbeeld maakt een executable `helloworld` welke bij het uitvoeren de `main` functie uitvoert in de `helloworld.__main__` module.

- install_requires=['boto3]<br/>
  Dit is een lijst van python packages welke meegeïnstalleerd moeten worden bij het installeren van de package. Dit zijn de run-time afhankelijkheden en niet de develop time afhankelijkheden zoals autopep8 en pytest. Die wil je alleen in je lokale develop virtualenv.

[Setuptools documentatie](https://setuptools.readthedocs.io/en/latest/index.html)

#### Installeer je package in edital mode

Ga in je source dir staan waar je setup.py staat en installeer je package in editable mode.

```
cd python
pip install -e .
```

De edital mode zorgt ervoor dat wijzigingen in je code ook direct effect hebben op het uitvoeren van een eventuele entrypoint.

### Main function en entrypoint

Om van je Python package een binary ofwel een executable te maken dien je in de `setup.py` een entrypoint op te nemen. Die moet verwijzen naar een functie die moet worden uitgevoerd bij het uitvoeren van je binary.

Deze functie is over het algemeen de main function in een `__main__.py` module.

#### argpparser

Een handige module is de argparser. Deze Helpt bij het afhandelen van commandline parameters.

[argparser documentatie](https://docs.python.org/3/library/argparse.html)

### Voorbeeld `__main__.py`

Hieronder een voorbeeld van een `__main__.py` met een argparser.

```python
import argparse
import sys
from helloworld.__init__ import version as __version__
from helloworld.log import logging
from helloworld.hello import Hello


def main(args=None):

    if args is None:
        args = sys.argv[1:]
    parser = get_parser()
    parsed_args = parser.parse_args(args)
    set_root_loglevel(parsed_args.verbose_count)

    if hasattr(parsed_args, "func"):
        parsed_args.func(parsed_args)
    else:
        parser.print_help()


def get_parser():
    parser = argparse.ArgumentParser(
        prog='helloworld', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v", "--verbose", dest="verbose_count",
                        action="count", default=0,
                        help="increases log verbosity for each occurence.")

    parser.add_argument('--version', action='version',
                        version='%(prog)s {version}'.format(version=__version__))

    subparsers = parser.add_subparsers(help='sub-command help')

    # Just Say Something

    parser_say = subparsers.add_parser(
        'say', help='say -h', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_say.add_argument('-t', '--text',
                                   help='Text to display', type=str, required=True)
    parser_say.set_defaults(func=say)

    return parser


def set_root_loglevel(verbose_count):
    logging.getLogger().setLevel(
        max(2 - verbose_count, 0) * 10)


def say(args):
    try:
        hello = Hello()
        hello.say(args.text)
        pass
    except Exception as err:
        logging.getLogger(__name__).error(err)
        sys.exit(1)


if __name__ == "__main__":
    main()
```
