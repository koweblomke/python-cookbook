import argparse
import sys
from helloworld.__init__ import version as __version__
from helloworld.log import logging
from helloworld.hello import Hello


def main(args=None):

    if args is None:
        args = sys.argv[1:]
    parser = get_parser()
    parsed_args = parser.parse_args(args)
    set_root_loglevel(parsed_args.verbose_count)

    if hasattr(parsed_args, "func"):
        parsed_args.func(parsed_args)
    else:
        parser.print_help()


def get_parser():
    parser = argparse.ArgumentParser(
        prog='helloworld', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v", "--verbose", dest="verbose_count",
                        action="count", default=0,
                        help="increases log verbosity for each occurence.")

    parser.add_argument('--version', action='version',
                        version='%(prog)s {version}'.format(version=__version__))

    subparsers = parser.add_subparsers(help='sub-command help')

    # Just Say Something

    parser_say = subparsers.add_parser(
        'say', help='say -h', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_say.add_argument('-t', '--text',
                                   help='Text to display', type=str, required=True)
    parser_say.set_defaults(func=say)

    return parser


def set_root_loglevel(verbose_count):
    logging.getLogger().setLevel(
        max(2 - verbose_count, 0) * 10)


def say(args):
    try:
        hello = Hello()
        hello.say(args.text)
        pass
    except Exception as err:
        logging.getLogger(__name__).error(err)
        sys.exit(1)


if __name__ == "__main__":
    main()