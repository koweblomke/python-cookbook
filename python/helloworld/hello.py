from helloworld.log import logging


class Hello:
    
    _logger = logging.getLogger(__name__)

    def __init__(self):
        self._logger.debug("Hello constructor called")
        pass

    def say(self, text):
        self._logger.debug("Hello.say called")
        print(text)
