import logging
import re
import os
from logging.config import dictConfig


class SensitiveFormatter(logging.Formatter):
    """Formatter that removes sensitive information in urls, used by logger in cli"""
    @staticmethod
    def _filter(txt):
        return re.sub(r'-----BEGIN ([\sA-Z]+)-----[^-]+-----END ([\sA-Z]+)-----',
                      r'-----BEGIN \1-----###REDACTED###-----END \2-----', txt, flags=re.DOTALL)

    def format(self, record):
        original = logging.Formatter.format(self, record)
        return self._filter(original)


LOG_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'datefmt': '%Y-%m-%d %H:%M:%S',
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
        'detail': {
            'datefmt': '%Y-%m-%d %H:%M:%S',
            'format': '%(asctime)s - [%(levelname)s] - [%(module)s.%(funcName)s:%(lineno)d]: %(message)s',
            '()': SensitiveFormatter
        },
    },
    'handlers': {
        'console': {
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
        },
        'file': {
            'level': 'DEBUG',
            'formatter': 'detail',
            'class': 'logging.FileHandler',
            'filename': 'output.log',
        },
        'audit': {
            'level': 'INFO',
            'formatter': 'standard',
            'class': 'logging.FileHandler',
            'filename': 'audit.log',
        }
    },
    'loggers': {
        '': {
            'handlers': ['console', 'file', 'audit'],
            'level': os.environ.get("LOG_LEVEL", "INFO").upper(),
            'propagate': True
        },
    }
}

dictConfig(LOG_CONFIG)

logging.debug('Logging initialized')
