attrs==19.3.0
autopep8==1.5.2
boto3==1.12.44
botocore==1.15.44
docutils==0.15.2
importlib-metadata==1.6.0
jmespath==0.9.5
more-itertools==8.2.0
packaging==20.3
pluggy==0.13.1
py==1.8.1
pycodestyle==2.5.0
pyparsing==2.4.7
pytest==5.4.1
python-dateutil==2.8.1
s3transfer==0.3.3
six==1.14.0
urllib3==1.25.9
wcwidth==0.1.9
zipp==3.1.0
