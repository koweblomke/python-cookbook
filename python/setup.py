from setuptools import setup
from subprocess import check_output
import helloworld


setup(name='hello-world',
      description='Python package with utility functions to great earth',
      version=helloworld.version,
      url='git@gitlab.com:koweblomke/python-cookbook.git',
      author='Rene Visser',
      author_email='koweblomke@gmail.com',
      license='MIT',
      packages=['helloworld'],
      entry_points={'console_scripts': ['helloworld=helloworld.__main__:main']},
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'boto3'
      ])